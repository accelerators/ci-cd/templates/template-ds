#include <tango.h>
#include <...Class.h>



/**
 *	Create Class singletons and store them in DServer object.
 */

void Tango::DServer::class_factory()
{

	add_class(..._ns::...Class::init("..."));
}
