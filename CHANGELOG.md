# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).










# Template to add section...

## [Unreleased]

## [X.X.X] - 201X-XX-XX
### Added
* For new functionality/features/code..
### Changed
* For changes in existing functionality.
### Deprecated
* For soon-to-be removed features.
### Removed
* For now removed functionality/features/code.
### Fixed
* For any bug fixes.
### Security
* In case of vulnerabilities.
