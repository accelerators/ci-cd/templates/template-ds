# Project Name

A short summary of what the project is and does.

## Cloning **(Update or Delete If Does Not Apply)**

Any instructions or special flags required to clone the project. For example, if the project has submodules in git, then specify this and give the command to check them out correctly, example:

```
git clone --recurse-submodules git@gitlab.esrf.fr:project/path/your_project_name.git
```

## Documentation **(Update or Delete If Does Not Apply)**

Links to internal/external documentation. Example, links to firmware documents, links to library references etc. You can link to a doc folder in the project, or mention it exists.

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies **(Update or Delete If Does Not Apply)**

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.
* Any other dependencies  **(Update or Delete If Does Not Apply)**

#### Toolchain Dependencies **(Update or Delete If Does Not Apply)**

* C++11 compliant compiler.
* CMake 16.0 or greater is required to perform the build. **(Update or Delete If Does Not Apply)**
* Any other dependencies  ***(Update or Delete If Does Not Apply)**


### Build

Instructions on building the project.

CMake example: **(Delete If Does Not Apply)**

```bash
cd project_name
cmake -S . -B build
cmake --build build --parallel $(nproc)
cmake --install build --prefix build/install
tree --du -h -C build/install
```

CMake example at ESRF: 
with TANGO_VERSION in [ "tango-9.3", "tango-9.5", "tango-10.0"  ]

```bash
cd project_name
cmake -S <source dir> -B <build dir> -DUSER_CONFIG=/opt/os_dev/cpp/<TANGO_VERSION>.cmake 
cmake --build <build dir> --parallel $(nproc)
cmake --install <build dir> --prefix <install dir>
tree --du -h -C <install dir>
```


