# Example of centralized configuration file
# ESRF CMAKE configuration
msg(DEBUG ">> Load module ESRF-config")
set(TANGO_VERS tango-9.5)

###########################################################################################
macro(configProject)

    msg(DEBUG "Set ESRF config for project")

    set(CMAKE_SHARED_LIBRARY_PREFIX libTC_)

    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        set(BUILD_TYPE_DIR "debug")
    else()
        set(CMAKE_BUILD_TYPE RelWithDebInfo)
        set(BUILD_TYPE_DIR "release")
    endif()

    printDebug(CMAKE_BUILD_TYPE)
    printDebug(ZMQ_DIR)
    printDebug(CPPZMQ_DIR)
    printDebug(OMNIORB_DIR)
    printDebug(CPPTANGO_DIR)
    printDebug(ACU_DIR)

    set(REPO_BUILD "/opt/os_dev/cpp/${BUILD_TYPE_DIR}")

    set(USER_TANGO_CLASSES_SEARCH_DIR "${REPO_BUILD}/${TANGO_VERS}/classes")
    set(USER_TANGO_LIB_SEARCH_DIR "${REPO_BUILD}/${TANGO_VERS}/lib")
    set(USER_LIB_SEARCH_DIR "${REPO_BUILD}/lib")

    # Set envirronment variable if not already set
    #setVarEmpty(CPPTANGO_VERSION 9.5)
    #setVarEmpty(ZMQ_VERSION      4.3)
    #setVarEmpty(CPPZMQ_VERSION   4.10)
    #setVarEmpty(OMNIORB_VERSION  4.3)
    #setVarEmpty(JPEG_VERSION     3.0)
    #setVarEmpty(GTEST_VERSION    1.14)
    
    setVarEmpty(ZMQ_DIR      "${USER_TANGO_LIB_SEARCH_DIR}/zmq")
    setVarEmpty(CPPZMQ_DIR   "${USER_TANGO_LIB_SEARCH_DIR}/cppzmq")   
    setVarEmpty(OMNIORB_DIR  "${USER_TANGO_LIB_SEARCH_DIR}/omniORB")
    setVarEmpty(JPEG_DIR     "${USER_TANGO_LIB_SEARCH_DIR}/jpeg-turbo")   
    setVarEmpty(CPPTANGO_DIR "${USER_TANGO_LIB_SEARCH_DIR}/cpptango")   
    setVarEmpty(GTEST_DIR    "${USER_TANGO_LIB_SEARCH_DIR}/googletest")

    set(PERMISSIONS_LIB
        OWNER_READ OWNER_WRITE 
        GROUP_READ GROUP_WRITE
        WORLD_READ
    )

    set(PERMISSIONS_BIN
        OWNER_READ OWNER_WRITE OWNER_EXECUTE
        GROUP_READ GROUP_WRITE GROUP_EXECUTE
        WORLD_READ             WORLD_EXECUTE
    )

    set(COMPILE_FEATURES )

    set(COMPILE_OPTIONS
        #-Werror                    # Make all warnings into errors.
        -Wall                       # This enables all the warnings.
        -Wextra                     # This enables some extra warning flags. 
        -Wpedantic                  # Issue all the warnings demanded by strict ISO C and ISO C++
        -Wnon-virtual-dtor          # warns whenever a class with virtual function does not declare a virtual destructor.
        -Wshadow                    # Warn whenever a local variable or type declaration shadows another variable
    )

endmacro()

###########################################################################################
macro(configTangoClass)

    msg(DEBUG "Set ESRF config for Tango Class")

    # Update install dir
    set(CMAKE_INSTALL_LIBDIR        classes/${PROJECT_NAME}/${PROJECT_VERSION}/lib)
    set(CMAKE_INSTALL_INCLUDEDIR    classes/${PROJECT_NAME}/${PROJECT_VERSION}/include)
    set(CMAKE_INSTALL_CMAKEDIR      classes/${PROJECT_NAME}/${PROJECT_VERSION}/cmake)
    set(CMAKE_INSTALL_SOURCEDIR     classes/${PROJECT_NAME}/${PROJECT_VERSION}/src)
    set(CMAKE_INSTALL_DATADIR       classes/${PROJECT_NAME}/${PROJECT_VERSION}/share)

    set(OUTPUT_NAME             ${PROJECT_NAME})
    set(OUTPUT_NAME_RELEASE     ${PROJECT_NAME})
    set(OUTPUT_NAME_DEBUG       ${PROJECT_NAME})

endmacro()

###########################################################################################
macro(configTangoLib)

    msg(DEBUG "Set ESRF config for Tango Library")

    # Update install dir
    set(CMAKE_INSTALL_LIBDIR        lib/${PROJECT_NAME}/${PROJECT_VERSION}/lib)
    set(CMAKE_INSTALL_INCLUDEDIR    lib/${PROJECT_NAME}/${PROJECT_VERSION}/include)
    set(CMAKE_INSTALL_CMAKEDIR      lib/${PROJECT_NAME}/${PROJECT_VERSION}/cmake)
    set(CMAKE_INSTALL_SOURCEDIR     lib/${PROJECT_NAME}/${PROJECT_VERSION}/src)
    set(CMAKE_INSTALL_DATADIR       lib/${PROJECT_NAME}/${PROJECT_VERSION}/share)

    set(OUTPUT_NAME             ${PROJECT_NAME})
    set(OUTPUT_NAME_RELEASE     ${PROJECT_NAME})
    set(OUTPUT_NAME_DEBUG       ${PROJECT_NAME})

endmacro()

###########################################################################################
macro(configTangoServer)

    msg(DEBUG "Set ESRF config for Tango Server")

    # Update install dir
    set(CMAKE_INSTALL_BINDIR    servers/${PROJECT_NAME}/${PROJECT_VERSION})

    # ESRF Specific binary name
    set(OUTPUT_NAME             ${PROJECT_NAME})
    set(OUTPUT_NAME_RELEASE     ${PROJECT_NAME})
    set(OUTPUT_NAME_DEBUG       ${PROJECT_NAME})

endmacro()

###########################################################################################
macro(configTangoBin)

    msg(DEBUG "Set ESRF config for Tango Executable")

    # Update install dir
    set(CMAKE_INSTALL_BINDIR    bin/${PROJECT_NAME}/${PROJECT_VERSION})

    # ESRF Specific binary name
    set(OUTPUT_NAME             ${PROJECT_NAME})
    set(OUTPUT_NAME_RELEASE     ${PROJECT_NAME})
    set(OUTPUT_NAME_DEBUG       ${PROJECT_NAME})

endmacro()
