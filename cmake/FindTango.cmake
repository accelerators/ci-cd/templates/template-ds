###############################################################################
# CMAKE POGO FILE (generated by pogo do not modify it)

msg(DEBUG ">> Load module FindTango")

###############################################################
# This CMake module produce some variables:
# TARGETS_CPPZMQ    set with Target name for ZeroMQ
# TARGETS_JPEG      set with Target name for Jpeg-Turbo
# TARGETS_CPPTANGO  set with Target name for cppTango
# TARGETS_TANGO     append with previous Targets
set(TARGETS_CPPZMQ )
set(TARGETS_JPEG )
set(TARGETS_CPPTANGO )
set(LINK_LIBRARIES_TANGO )

# Detect some system environment variables and if existing, recreate them as CMake variables
# to use them for config

setEnv2Cmake(CPPZMQ_VERSION)
setEnv2Cmake(CPPZMQ_DIR)

setEnv2Cmake(ZMQ_VERSION)
setEnv2Cmake(ZMQ_DIR)

setEnv2Cmake(OMNIORB_VERSION)
setEnv2Cmake(OMNIORB_DIR)

setEnv2Cmake(CPPTANGO_VERSION)
setEnv2Cmake(CPPTANGO_DIR)

setEnv2Cmake(JPEG_VERSION)
setEnv2Cmake(JPEG_DIR)


#####################################
# Get Interface Target for Threads
find_package(Threads REQUIRED)
list(APPEND LINK_LIBRARIES_TANGO Threads::Threads)

#####################################
# Get Interface Target for ZeroMQ
# find_package() -> libzmq;libzmq-static;cppzmq;cppzmq-static
# pkg_check_modules -> PkgConfig::libzmq

findLibConfig(
    QUIET
    NAME cppzmq
    NAME_CMAKE cppzmq
    NAME_PKG-CONFIG libzmq
    VERSION ${CPPZMQ_VERSION}
    SEARCH_DIRS ${ZMQ_DIR};${CPPZMQ_DIR}
    EXPECTED_TARGETS cppzmq
    OUTPUT_LIST TARGETS_CPPZMQ
    OUTPUT_LIST_APPEND LINK_LIBRARIES_TANGO
)


#####################################
# Get Interface Target for JPeg-Turbo
# find_package() -> libjpeg-turbo::jpeg;libjpeg-turbo::turbojpeg;libjpeg-turbo::turbojpeg-static;libjpeg-turbo::jpeg-static
# pkg_check_modules -> PkgConfig::libjpeg

findLibConfig(
    QUIET
    NAME jpeg
    NAME_CMAKE JPEG
    NAME_PKG-CONFIG libjpeg
    VERSION ${JPEG_VERSION}
    SEARCH_DIRS ${JPEG_DIR}
    EXPECTED_TARGETS JPEG::JPEG
    OUTPUT_LIST TARGETS_JPEG
    OUTPUT_LIST_APPEND LINK_LIBRARIES_TANGO
)


#####################################
# Get Interface Target for cppTango
# find_package() -> 
# pkg_check_modules -> PkgConfig::tango

findLibConfig(
    QUIET
    REQUIRED
    NAME tango
    NAME_CMAKE tango
    NAME_PKG-CONFIG tango
    VERSION ${CPPTANGO_VERSION}
    SEARCH_DIRS ${ZMQ_DIR};${CPPZMQ_DIR};${OMNIORB_DIR};${CPPTANGO_DIR};${JPEG_DIR}
    EXPECTED_TARGETS tango::tango
    OUTPUT_LIST TARGETS_CPPTANGO
    OUTPUT_LIST_APPEND LINK_LIBRARIES_TANGO
)


#####################################
# Show recovered Targets information

msg(SECTION "DISPLAY IMPORTED TARGET content for: ${LINK_LIBRARIES_TANGO}")
foreach(TARGET_NAME ${LINK_LIBRARIES_TANGO}) 
    showTargetProperties(${TARGET_NAME})
endforeach()
